
// Run local mqtt server
$ sudo apt install mosquitto mosquitto-clients
$ mosquitto
$ mosquitto_sub -t "s/us"


// Run python script to monitor mqtt packets
$ pip3 install numpy
$ pip3 install paho-mqtt
$ python3 [PATH_TO_REPO]/gw_parser/gw_parser.py


// Monitor output of beacons (optional)
Open serial connection at 115200 on the DK USB-Serial COM port
