/** @file
 *
 * @defgroup ble_contact_tracer main.c
 * @{
 * @brief BLE Beacon/Scanner Sample Application main file.
 *
 */
 
#include <stdbool.h>
#include <stdint.h>
#include <stdio.h>
#include <string.h>

#include "nrf_delay.h"
#include "nrf_sdh.h"
#include "nrf_sdh_ble.h"
#include "nrf_sdh_soc.h"
#include "nrf_pwr_mgmt.h"
#include "app_timer.h"
#include "boards.h"
#include "bsp.h"
#include "bsp_btn_ble.h"
#include "ble.h"
#include "ble_conn_state.h"
#include "ble_hci.h"
#include "ble_advertising.h"
#include "ble_conn_params.h"

#include "nrf_ble_gatt.h"
#include "nrf_ble_scan.h"

#include "nrf_log.h"
#include "nrf_log_ctrl.h"
#include "nrf_log_default_backends.h"

#include "nordic_common.h"
#include "nrf_soc.h"
#include "nrf_sdh.h"
#include "ble_advdata.h"

#define HEARTBEAT_LED            BSP_BOARD_LED_0                     /**< Scanning LED will be on when the device is scanning. */
#define INDICATION_LED           BSP_BOARD_LED_1                     /**< Connected LED will be on when the device is connected. */
#define LEDBUTTON_LED                   BSP_BOARD_LED_2                     /**< LED to indicate a change of state of the the Button characteristic on the peer. */

#define APP_BLE_CONN_CFG_TAG            1                                  /**< A tag identifying the SoftDevice BLE configuration. */

#define NON_CONNECTABLE_ADV_INTERVAL    MSEC_TO_UNITS(100, UNIT_0_625_MS)  /**< The advertising interval for non-connectable advertisement (100 ms). This value can vary between 100ms to 10.24s). */

#define APP_COMPANY_IDENTIFIER          0x0059
#define DEAD_BEEF                       0xDEADBEEF                         /**< Value used as error code on stack dump, can be used to identify stack location on stack unwind. */

#define APP_BLE_OBSERVER_PRIO           3                                  /**< Application's BLE observer priority. You shouldn't need to modify this value. */

#define CT_TRACER_APP_ID                0xB1, 0xED   // Magic number for contract tracer app
#define CT_TRACER_APP_ID_OFFSET         0x00

#define CT_TRACER_DEVICE_ID             'C'         // This devices identifier
#define CT_TRACER_DEVICE_ID_OFFSET      0x02

#define CT_TRACER_NUM_CONTACTS          0x00         // Number of other devices this device has contacted
#define CT_TRACER_NUM_CONTACTS_OFFSET   0x03

#define CT_TRACER_CONTACT_LIST_OFFSET   0x04

#define MANUFACTURER_ID_LEN             2

#define MAX_CONTACTS_SEEN               5
#define CT_TRACER_DATA_LENGTH           23
#define CT_RSSI_THRESHOLD               -70     //RSSI must be atleast this to count as contact
#define CT_CONTACT_TIME_THRESHOLD_MS    10000   //This must time must pass since last contact for contact to count again

#define DEVICE_NAME_MAX_SIZE 20
#define DEVICE_TO_FIND_MAX 50

#define SCAN_LIST_REFRESH_INTERVAL 10000  // 10 sec
#define FOUND_DEVICE_REFRESH_TIME APP_TIMER_TICKS(SCAN_LIST_REFRESH_INTERVAL) /**< Time after which the device list is clean and refreshed. */

NRF_BLE_SCAN_DEF(m_scan);                                                  /**< Scanning module instance. */
NRF_BLE_GATT_DEF(m_gatt);                                                  /**< GATT module instance. */

static uint32_t             device_number;
static ble_gap_adv_params_t m_adv_params;                                  /**< Parameters to be passed to the stack when starting advertising. */
static uint8_t              m_adv_handle = BLE_GAP_ADV_SET_HANDLE_NOT_SET; /**< Advertising handle used to identify an advertising set. */
static uint8_t              m_enc_advdata[BLE_GAP_ADV_SET_DATA_SIZE_MAX][2];  /**< Buffer for storing an encoded advertising set. */


static volatile uint32_t timestamp_tick_10_ms; //global tick counter, each tick is 10ms 

/**@brief Struct that contains pointers to the encoded advertising data. 

  must use 2 sets of buffers to update advertising data while advertising
*/

static int active_buffer = 0;

static ble_gap_adv_data_t m_adv_data[2] =
{
  {
    .adv_data =
    {
        .p_data = m_enc_advdata[0],
        .len    = BLE_GAP_ADV_SET_DATA_SIZE_MAX
    },
    .scan_rsp_data =
    {
        .p_data = NULL,
        .len    = 0

    }
  },
  {
    .adv_data =
    {
        .p_data = m_enc_advdata[1],
        .len    = BLE_GAP_ADV_SET_DATA_SIZE_MAX
    },
    .scan_rsp_data =
    {
        .p_data = NULL,
        .len    = 0

    }
  }
};

/* Look up table for the timestmap a tracer was last seen */
static uint32_t last_seen_lut[256] = {0};

typedef struct
{ 
  uint8_t name;
  uint8_t count;
} ct_data_t;

static uint8_t ct_tracer_data[CT_TRACER_DATA_LENGTH] =                    /**< Information advertised by the Contact Tracer. */
{
  CT_TRACER_APP_ID,      //Identifies this as a Contact tracer
  CT_TRACER_DEVICE_ID,
  CT_TRACER_NUM_CONTACTS,
};

typedef struct
{
    bool      is_not_empty;                                   /**< Indicates that the structure is not empty. */
    uint16_t  size;                                           /**< Size of manuf data. */
    uint8_t   addr[BLE_GAP_ADDR_LEN];                         /**< Device address. */
    char      dev_name[DEVICE_NAME_MAX_SIZE];                 /**< Device name. */
    uint8_t   manuf_buffer[BLE_GAP_ADV_SET_DATA_SIZE_MAX];    /**< Buffer for storing an  manuf data. */

} scanned_device_t;

scanned_device_t   m_device[DEVICE_TO_FIND_MAX];                 /**< Stores device info from scan data. */


static void advertising_data_update(void);
static void device_list_print(scanned_device_t * p_device);
static void scan_evt_handler(scan_evt_t const * p_scan_evt);
static void ble_evt_handler(ble_evt_t const * p_ble_evt, void * p_context);
static uint32_t get_timestmap( void );

/* Check if the manufacturing data contains valid contact tracer data */
static bool ct_manuf_data_valid(uint8_t * data, int len)
{
  uint8_t app_id_magic_num[2] = {CT_TRACER_APP_ID};
  if (len < 6)
  {
    return false;
  }

  if (data[2] == app_id_magic_num[0] && data[3] == app_id_magic_num[1])
  {
    return true;
  }
  return false;
}

/* Get the single character device id from the manufacturing data */
static char ct_manuf_data_get_name( uint8_t * data)
{
  return data[CT_TRACER_DEVICE_ID_OFFSET + MANUFACTURER_ID_LEN];
}

/* Add a new contact with contact_name to our manufacturing data */
static int ct_device_add_contact ( uint8_t * data, char contact_name)
{
  uint8_t num_contacts = ct_tracer_data[CT_TRACER_NUM_CONTACTS_OFFSET];
  uint8_t offset = CT_TRACER_CONTACT_LIST_OFFSET;
  for (int i = 0; i < num_contacts; i++)
  {

    ct_data_t * contact_data = (ct_data_t*) &data[offset];
    /* Found this contact in the list */
    if ( contact_name == contact_data->name)
    {
      /* Increment the contact counter for this id */  
      if (contact_data->count < 255)
      {
        contact_data->count++;
      }
      return 0;
    }

    /* Move 2 bytes to the next list entry */
    offset += sizeof(ct_data_t);
  }

  /* Handle case of new contact */
  if (num_contacts < MAX_CONTACTS_SEEN)
  {
    ct_data_t * contact_data = (ct_data_t*) &data[CT_TRACER_CONTACT_LIST_OFFSET] + num_contacts;
    contact_data->name = contact_name;
    contact_data->count = 1;
    ct_tracer_data[CT_TRACER_NUM_CONTACTS_OFFSET]++;
    return 0;
  }

  return -1;
}

/* Check whether to update this device in the list based on time last seen and RSSI 
  true = update this device, false = ignore
*/
static bool ct_device_filter (char name, int8_t rssi, uint32_t timestamp)
{
  uint32_t now = get_timestmap();
  
  /* Check if it meets the RSSI Threshold */
  if (rssi > CT_RSSI_THRESHOLD)
  {
    
    bsp_board_led_on(INDICATION_LED);
    nrf_delay_ms(200);
    bsp_board_led_off(INDICATION_LED);
    nrf_delay_ms(200);
    
    /* Look up when this contact was last seen */
    uint32_t last_seen = last_seen_lut[name];
    if ((last_seen == 0) || ((now - last_seen) > CT_CONTACT_TIME_THRESHOLD_MS))
    {
      return true;
    }
    {
      NRF_LOG_WARNING("Not enough time passed");
    }
  }
  else 
  {
    NRF_LOG_WARNING("RSSI too low");
  }
  return false;
}

static int ct_handle_tracer(uint8_t * data, int data_len, int8_t rssi, uint32_t timestamp)
{
  /* Check if this is valid contact tracer manufacturing data */
  if (ct_manuf_data_valid(data, data_len))
  {
    /* Get name for device */
    char name = ct_manuf_data_get_name(data);
    
    if (ct_device_filter(name, rssi, timestamp) ==  true)
    {
      if (ct_device_add_contact(ct_tracer_data, name) == 0)
      {
        NRF_LOG_INFO("Succesfully updated contact list");
        last_seen_lut[name] = timestamp; //update timestamp for this device
        advertising_data_update();
        return 0;
      }
      else 
      {
        NRF_LOG_INFO("Failed to add contact");
      }
    }
    else
    {
      NRF_LOG_INFO("Tracker did not pass filter");
    }
  }
  return -1;
}

/*
 *
 *            Helper Functions 
 *
 */

void scan_device_info_clear(void)
{
    memset(m_device, 0, sizeof(m_device));
    device_number = 0;
}

/**@brief Function for printing the devices.
 *
 *@details Function print list of devices.
 *      
 *
 * @param[in] device      Pointer to the struct storing the scanned devices.
 */
static void device_list_print(scanned_device_t * p_device)
{
    NRF_LOG_INFO(" ----- Num devices = %d -----\r\n\n", device_number);

    // We could now clear the list:
    scan_device_info_clear();
}


scanned_device_t * scan_device_info_get(void)
{
    return m_device;
}

typedef struct
{
    uint8_t * p_data;   /**< Pointer to data. */
    uint16_t  data_len; /**< Length of data. */
} data_t;

#if 0
static void print_buff ( char * header, const uint8_t * buff, uint8_t len, bool string_mode )
{
  int ii = 0;
  static char tempBuff[512] = "";
  char * pBuff = &tempBuff[0];
  
  if ( len == 0 )
  {
    pBuff += sprintf(pBuff, "%s No Data", header);
  }
  else
  {
    pBuff += sprintf(pBuff, "[%d] %s", len, header);
    
    for ( ii = 0; ii < len; ii++ )
    {
      if ( string_mode )
      {
        pBuff += sprintf(pBuff, "%c", buff[ii]);
      }
      else
      {
        pBuff += sprintf(pBuff, "x%x,", buff[ii]);
      }
    }
  }
  
  NRF_LOG_INFO("%s", tempBuff);
}
#endif

#if 0
static void print_type ( ble_gap_adv_report_type_t const * type )
{
  if ( type->connectable ) NRF_LOG_INFO("connectable = %d", type->connectable );
  if ( type->scannable ) NRF_LOG_INFO("scannable = %d", type->scannable );
  if ( type->directed ) NRF_LOG_INFO("directed = %d", type->directed );
  if ( type->scan_response ) NRF_LOG_INFO("scan_response = %d", type->scan_response );
  if ( type->extended_pdu ) NRF_LOG_INFO("extended_pdu = %d", type->extended_pdu );
  if ( type->status ) NRF_LOG_INFO("status = %d", type->status );
}
#endif

static int extract_manuf_data ( ble_gap_evt_adv_report_t const * p_adv_report, uint8_t * out_buffer)
{
  uint16_t   manf_offset = 0;
  uint16_t   manf_len    = 0;

  const ble_data_t *data = &p_adv_report->data;

  manf_len = ble_advdata_search(data->p_data, data->len, &manf_offset, BLE_GAP_AD_TYPE_MANUFACTURER_SPECIFIC_DATA);
  memcpy(out_buffer, &data->p_data[manf_offset], manf_len);
  return manf_len;
}

#if 0
static void extract_name ( ble_gap_evt_adv_report_t const * p_adv_report )
{
  uint16_t    name_len;
  uint16_t    name_offset = 0;

  const ble_data_t *data = &p_adv_report->data;

  name_len = ble_advdata_search(data->p_data, data->len, &name_offset, BLE_GAP_AD_TYPE_COMPLETE_LOCAL_NAME);
  print_buff ("Name = ", &data->p_data[name_offset], name_len, true);
}


static void extract_address ( ble_gap_evt_adv_report_t const * p_adv_report )
{
  print_buff ("Addr = ", p_adv_report->peer_addr.addr, BLE_GAP_ADDR_LEN, false);
}
#endif

static void ct_handle_adv_report ( ble_gap_evt_adv_report_t const * p_adv_report )
{
  //const ble_data_t *data = &p_adv_report->data;
  uint8_t manuf_data[BLE_GAP_ADV_SET_DATA_SIZE_MAX];
  int manuf_data_len = 0;

  manuf_data_len = extract_manuf_data( p_adv_report, manuf_data);
  if (ct_manuf_data_valid( manuf_data, manuf_data_len) == true)
  {
    char name = ct_manuf_data_get_name(manuf_data);
    uint32_t timestamp = get_timestmap();
    NRF_LOG_INFO("Found Contact Tracer! Device ID = [%c]. RSSI = (%ddB). Time = %d", name, p_adv_report->rssi, timestamp );
    ct_handle_tracer( manuf_data, manuf_data_len, p_adv_report->rssi, timestamp);
  }
  else 
  {
    NRF_LOG_INFO("Found other device. RSSI = (%ddB)", p_adv_report->rssi );
  }

  // extract_name(p_adv_report);
  // extract_address(p_adv_report);
  // print_type(&p_adv_report->type);
}

static void device_found ( ble_gap_evt_adv_report_t const * p_adv_report )
{
    uint8_t  idx             = 0;

    for ( idx = 0; idx < DEVICE_TO_FIND_MAX; idx++)
    {
        // If address is duplicated, then return.
        if (memcmp(p_adv_report->peer_addr.addr, m_device[idx].addr, sizeof(p_adv_report->peer_addr.addr)) == 0)
        {
            return;
        }
    }

    // Device is not in the list.
    for (idx = 0; idx < DEVICE_TO_FIND_MAX; idx++)
    {
      if (!m_device[idx].is_not_empty) // We find an empty entry
      {
        device_number = device_number + 1;
        m_device[idx].is_not_empty = true; //validating the list record
        memset(m_device[idx].addr, 0, sizeof(p_adv_report->peer_addr.addr));
        memcpy(m_device[idx].addr, p_adv_report->peer_addr.addr, sizeof(p_adv_report->peer_addr.addr));
        
        ct_handle_adv_report(p_adv_report);
        return;
          
      }
    }
}

/* Returns timestamp in ms */
static uint32_t get_timestmap( void )
{
  return timestamp_tick_10_ms*10;
}

static void tick_handler(void * p_context)
{
  timestamp_tick_10_ms++;
}


/**@brief Function for handling the list_timer event,
 */
static void adv_list_timer_handle(void * p_context)
{
        
    // Print devices
    scanned_device_t * p_device_list = scan_device_info_get();
    device_list_print(p_device_list);

    //scan_device_info_clear();
}





/*
 *
 *            Central Functions 
 *
 */

static void scan_init(void)
{
    ret_code_t          err_code;
    nrf_ble_scan_init_t init_scan;

    memset(&init_scan, 0, sizeof(init_scan));

    init_scan.connect_if_match = false;
    init_scan.conn_cfg_tag     = APP_BLE_CONN_CFG_TAG;

    err_code = nrf_ble_scan_init(&m_scan, &init_scan, scan_evt_handler);
    APP_ERROR_CHECK(err_code);
}


/**@brief Function to start scanning.
 */
static void scan_start(void)
{
    ret_code_t err_code;

    err_code = nrf_ble_scan_start(&m_scan);
    APP_ERROR_CHECK(err_code);
}


/**@brief Function for handling Scaning events.
 *
 * @param[in]   p_scan_evt   Scanning event.
 */
static void scan_evt_handler(scan_evt_t const * p_scan_evt)
{
    ret_code_t err_code;

    switch(p_scan_evt->scan_evt_id)
    {
        case NRF_BLE_SCAN_EVT_CONNECTING_ERROR:
            err_code = p_scan_evt->params.connecting_err.err_code;
            APP_ERROR_CHECK(err_code);
            break;

         case NRF_BLE_SCAN_EVT_NOT_FOUND:
             device_found(p_scan_evt->params.p_not_found);
             break;

          case NRF_BLE_SCAN_EVT_SCAN_REQ_REPORT:          
              NRF_LOG_INFO("--------------------- SCAN REQ--------------");
              break;

        default:
          NRF_LOG_INFO("--------------------- OTHER EVENT = %d --------------", p_scan_evt->scan_evt_id);
          break;
    }
}


/*
 *
 *            Peripheral Functions 
 *
 */


/**@brief Function for initializing the Advertising functionality.
 *
 * @details Encodes the required advertising data and passes it to the stack.
 *          Also builds a structure to be passed to the stack when starting advertising.
 */
static void advertising_init(void)
{
    uint32_t      err_code;
    ble_advdata_t advdata;
    uint8_t       flags = BLE_GAP_ADV_FLAG_BR_EDR_NOT_SUPPORTED;

    ble_advdata_manuf_data_t manuf_specific_data;

    manuf_specific_data.company_identifier = APP_COMPANY_IDENTIFIER;
    manuf_specific_data.data.p_data = (uint8_t *) ct_tracer_data;
    manuf_specific_data.data.size   = CT_TRACER_DATA_LENGTH;

    // Build and set advertising data.
    memset(&advdata, 0, sizeof(advdata));

    advdata.name_type             = BLE_ADVDATA_NO_NAME;
    advdata.flags                 = flags;
    advdata.p_manuf_specific_data = &manuf_specific_data;

    // Initialize advertising parameters (used when starting advertising).
    memset(&m_adv_params, 0, sizeof(m_adv_params));

    m_adv_params.properties.type = BLE_GAP_ADV_TYPE_NONCONNECTABLE_NONSCANNABLE_UNDIRECTED;
    m_adv_params.p_peer_addr     = NULL;    // Undirected advertisement.
    m_adv_params.filter_policy   = BLE_GAP_ADV_FP_ANY;
    m_adv_params.interval        = NON_CONNECTABLE_ADV_INTERVAL;
    m_adv_params.duration        = 0;       // Never time out.

    err_code = ble_advdata_encode(&advdata, m_adv_data[0].adv_data.p_data, &m_adv_data[0].adv_data.len);
    APP_ERROR_CHECK(err_code);

    err_code = sd_ble_gap_adv_set_configure(&m_adv_handle, &m_adv_data[0], &m_adv_params);
    APP_ERROR_CHECK(err_code);
}


/**@brief Function for starting advertising.
 */
static void advertising_start(void)
{
    ret_code_t err_code;

    err_code = sd_ble_gap_adv_start(m_adv_handle, APP_BLE_CONN_CFG_TAG);
    APP_ERROR_CHECK(err_code);

    err_code = bsp_indication_set(BSP_INDICATE_ADVERTISING);
    APP_ERROR_CHECK(err_code);
}

/* Update the advertising data */
static void advertising_data_update(void)
{
  ret_code_t err_code;
  int next_buffer = 0;
  ble_advdata_t advdata;
  uint8_t       flags = BLE_GAP_ADV_FLAG_BR_EDR_NOT_SUPPORTED;

  ble_advdata_manuf_data_t manuf_specific_data;

  manuf_specific_data.company_identifier = APP_COMPANY_IDENTIFIER;
  manuf_specific_data.data.p_data = (uint8_t *) ct_tracer_data;
  manuf_specific_data.data.size   = CT_TRACER_DATA_LENGTH;

  // Build and set advertising data.
  memset(&advdata, 0, sizeof(advdata));

  advdata.name_type             = BLE_ADVDATA_NO_NAME;
  advdata.flags                 = flags;
  advdata.p_manuf_specific_data = &manuf_specific_data;

  if (active_buffer == 0)
  {
    next_buffer = 1;
    active_buffer = 1;
  }
  else {
    next_buffer = 0;
    active_buffer = 0;
  }

  err_code = ble_advdata_encode(&advdata, m_adv_data[next_buffer].adv_data.p_data, &m_adv_data[next_buffer].adv_data.len);
  APP_ERROR_CHECK(err_code);

  err_code = sd_ble_gap_adv_set_configure(&m_adv_handle, &m_adv_data[next_buffer], NULL);
  APP_ERROR_CHECK(err_code);

}



/*
 *
 *            BLE Stack Functions 
 *
 */


/**@brief Function for initializing the BLE stack.
 *
 * @details Initializes the SoftDevice and the BLE event interrupt.
 */
static void ble_stack_init(void)
{
    ret_code_t err_code;

    err_code = nrf_sdh_enable_request();
    APP_ERROR_CHECK(err_code);

    // Configure the BLE stack using the default settings.
    // Fetch the start address of the application RAM.
    uint32_t ram_start = 0;
    err_code = nrf_sdh_ble_default_cfg_set(APP_BLE_CONN_CFG_TAG, &ram_start);
    APP_ERROR_CHECK(err_code);

    // Enable BLE stack.
    err_code = nrf_sdh_ble_enable(&ram_start);
    APP_ERROR_CHECK(err_code);

    // Register a handler for BLE events.
    NRF_SDH_BLE_OBSERVER(m_ble_observer, APP_BLE_OBSERVER_PRIO, ble_evt_handler, NULL);
}



/**@brief Function for handling BLE events.
 *
 * @param[in]   p_ble_evt   Bluetooth stack event.
 * @param[in]   p_context   Unused.
 */
static void ble_evt_handler(ble_evt_t const * p_ble_evt, void * p_context)
{
    uint16_t conn_handle = p_ble_evt->evt.gap_evt.conn_handle;
    uint16_t role        = ble_conn_state_role(conn_handle);

    // Based on the role this device plays in the connection, dispatch to the right handler.
    if (role == BLE_GAP_ROLE_PERIPH )
    {
      //Don't need to do anything
      //on_ble_peripheral_evt(p_ble_evt);
    }
    else if ((role == BLE_GAP_ROLE_CENTRAL) || (p_ble_evt->header.evt_id == BLE_GAP_EVT_ADV_REPORT))
    {
      //May not need to do anything
      //on_ble_central_evt(p_ble_evt);
    }
}






/*
 *
 *            Initialization Functions 
 *
 */


/**@brief Function for initializing logging. */
static void log_init(void)
{
    ret_code_t err_code = NRF_LOG_INIT(NULL);
    APP_ERROR_CHECK(err_code);

    NRF_LOG_DEFAULT_BACKENDS_INIT();
}

/**@brief Function for initializing LEDs. */
static void leds_init(void)
{
    ret_code_t err_code = bsp_init(BSP_INIT_LEDS, NULL);
    APP_ERROR_CHECK(err_code);
}


/**@brief Function for initializing timers. */
static void timers_init(void)
{
    ret_code_t err_code = app_timer_init();
    APP_ERROR_CHECK(err_code);

    // Timer for refreshing scanned devices data.
    APP_TIMER_DEF(adv_list_timer);
    err_code = app_timer_create(&adv_list_timer, APP_TIMER_MODE_REPEATED, adv_list_timer_handle);
    APP_ERROR_CHECK(err_code);

    err_code = app_timer_start(adv_list_timer, FOUND_DEVICE_REFRESH_TIME, NULL);
    APP_ERROR_CHECK(err_code);

    // Timer for refreshing scanned devices data.
    APP_TIMER_DEF(timestamp_timer);
    err_code = app_timer_create(&timestamp_timer, APP_TIMER_MODE_REPEATED, tick_handler);
    APP_ERROR_CHECK(err_code);

    err_code = app_timer_start(timestamp_timer, APP_TIMER_TICKS(10), NULL);
    APP_ERROR_CHECK(err_code);
}


/**@brief Function for initializing power management.
 */
static void power_management_init(void)
{
    ret_code_t err_code;
    err_code = nrf_pwr_mgmt_init();
    APP_ERROR_CHECK(err_code);
}


/**@brief Function for handling the idle state (main loop).
 *
 * @details If there is no pending log operation, then sleep until next the next event occurs.
 */
static void idle_state_handle(void)
{
    if (NRF_LOG_PROCESS() == false)
    {
        nrf_pwr_mgmt_run();
    }
}


/**
 * @brief Function for application main entry.
 */
int main(void)
{
    // Initialize.
    log_init();
    timers_init();
    leds_init();
    power_management_init();
    ble_stack_init();
    advertising_init();
    scan_init();

    // Start execution.
    NRF_LOG_INFO("BLE Contact Tracer Started."); 
    scan_start();
    advertising_start();

    // Enter main loop.
    for (;; )
    {
        idle_state_handle();
    }
}



/**@brief Callback function for asserts in the SoftDevice.
 *
 * @details This function will be called in case of an assert in the SoftDevice.
 *
 * @warning This handler is an example only and does not fit a final product. You need to analyze
 *          how your product is supposed to react in case of Assert.
 * @warning On assert from the SoftDevice, the system can only recover on reset.
 *
 * @param[in]   line_num   Line number of the failing ASSERT call.
 * @param[in]   file_name  File name of the failing ASSERT call.
 */
void assert_nrf_callback(uint16_t line_num, const uint8_t * p_file_name)
{
    app_error_handler(DEAD_BEEF, line_num, p_file_name);
}



/**
 * @}
 */
