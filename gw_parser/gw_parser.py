#pip3 install numpy
#pip3 install paho-mqtt

import paho.mqtt.client as mqtt #import the client1
import json
import numpy as np

broker_address="127.0.0.1"
subscribe_topic="s/us"
manf_id="5900"
secret_id="B1ED"
verbose =False
vVerbose=False

verboseprint = print if verbose else lambda *a, **k: None
vverboseprint = print if vVerbose else lambda *a, **k: None


def is_valid_beacon ( raw_data ):
    if raw_data.find(manf_id + secret_id) == -1:
        return False
    else:
        return True

def get_beacon_payloads(gw_payload):
    beacon_payloads = []

    for dev in gw_payload:
        rawData = dev.get("rawData", "Empty")
        
        if ( is_valid_beacon(rawData) ):
            verboseprint("Valid Device Data:   " + rawData)
            beacon_payloads.append(rawData)
        else:
            verboseprint("Invalid Device Data: " + rawData)

    return beacon_payloads



def get_ble_adv_packet_field ( packet, start ):
    print ("Packet")
    pLen  = packet[start + 0];
    print ("Len = " + pLen)
    pType = packet[start + 1];
    print ("Type = " + pType)
    pData = packet[start + 2:start + 2 + len - 1];
    print ("Data = " + pData)

'''
    Parse the BLE advertising packet raw data for the relevant fields and print them
'''
def print_payload_data( beacon_pld ):
    global secret_id
    global manf_id

    manf_data_start        = beacon_pld.find(manf_id + secret_id);
    secret_key_start       = manf_data_start + len(manf_id)
    device_id_data_start   = secret_key_start + len(secret_id)
    contact_count_start    = device_id_data_start + 2
    device_list_data_start = contact_count_start + 2
    
    manf_id     = beacon_pld[manf_data_start:manf_data_start + len(manf_id)]
    secret_key  = beacon_pld[secret_key_start:secret_key_start + len(secret_id)]
    device_id   = chr(int(beacon_pld[device_id_data_start:device_id_data_start+2], 16))
    contact_cnt = int(beacon_pld[contact_count_start:contact_count_start+2], 16)
    device_list = beacon_pld[device_list_data_start:]

    print(f"Device: \"{device_id}\" with {contact_cnt} contacts")

    d_array = bytearray.fromhex(device_list)
    if ( len(d_array)%2 != 0 ):
        d_array.pop()

    kv_devices = np.reshape(d_array, (-1, 2))

    for k,v in kv_devices:
      if ( k != 0 ):
        device_char = chr(k)
        print(f"Contacted Device: {device_char} ({v}) times")

    print("")


def on_message(client, userdata, message):
    #print("message received " ,str(message.payload.decode("utf-8")))
    #print("message topic=",message.topic)
    #print("message qos=",message.qos)
    #print("message retain flag=",message.retain)

    payload = json.loads(str(message.payload.decode("utf-8")))
    #print(payload)
    vverboseprint("Gateway Payload= ")
    vverboseprint(json.dumps(payload, indent=4, sort_keys=True))
    beacon_data = get_beacon_payloads(payload)
    
    #print_payload_data(beacon_data)

    for d in beacon_data:
        print_payload_data(d)

    print("--------------------------\r\n")


print("Starting Contact Tracer Demo")

verboseprint("creating new instance")
client = mqtt.Client("PyParser") #create new instance

verboseprint("Setting up callbacks")
client.on_message=on_message        #attach function to callback

verboseprint("connecting to broker")
client.connect(broker_address) #connect to broker

verboseprint("Subscribing to topic", subscribe_topic)
client.subscribe(subscribe_topic)

client.loop_start()

input("Press Enter to exit...\r\n\n")
print("----------- Exit ------------\r\n\n")

client.disconnect()


